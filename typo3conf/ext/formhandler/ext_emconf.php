<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "formhandler".
 *
 * Auto generated 03-09-2014 11:52
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Formhandler',
	'description' => 'The swiss army knife for all kinds of mailforms, completely new written using the MVC concept. Result: Flexibility, Flexibility, Flexibility	:-).',
	'category' => 'plugin',
	'version' => '2.0.1',
	'state' => 'stable',
	'uploadfolder' => false,
	'createDirs' => NULL,
	'clearcacheonload' => true,
	'author' => 'Dev-Team Typoheads',
	'author_email' => 'dev@typoheads.at',
	'author_company' => 'Typoheads GmbH',
	'constraints' => 
	array (
		'depends' => 
		array (
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

